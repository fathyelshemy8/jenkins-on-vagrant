# installing jenkins using vagrant with ubuntu
### what is vagrant?
```
    is a configration tool to manage and provision  VM  which can get vm on VMware or Oracle Virtual box with zero time to install and add some software we need on it  with zero time just  good connection speed and let your vagrantfile work  by some command 
```
## steps
* **download and install vagrant from offical website**
* **create vagrant file**
* **explain vagrantfile content**
* **How to run  VM**
* **ways to contact with vm**
-----------------------------------------------------------
### download and install

    you can download vagrant accourding to your system from [here](https://www.vagrantup.com/downloads.html) and run your downloaded source  to finish the installation and you hava to download your provider (VM containter) like VMWare or Oracle virtual box and i will you virtual box because it free and open source you can download it from [here](https://www.virtualbox.org/wiki/Downloads) choose your compitable version and install it 

### Create vagrant file ###
    after download  and installation  open your cmd or powershell in windows and type the following 
    ```
        vagrant --verion
    ```
    used to check for vagrant version

   #### to create first vagrant vm 
    * type the following with Ubuntu 16.4 LTS
        ```
            vagrant init ubuntu/xenial64
        ```

          now you have vagrant file in your current path called Vagrantfile
    * to start your machine 
        type the following command in your cmd or terminal
            ```
                vagrant up
            ```

        wait until the finish the process
        now you have ubuntu vm working in back ground 
    * to login your Ubuntu system
        vagrant us ssh to connect to your machine and interact with it sure you can make it work with Provider(Virtual and VMware)  GUI by do some configuration in Vagrantfile
        
            type following command to login to your system
                ```
                    vagrant ssh
                ```
        note:
            vagrant has it's private key to login without type password to your system  and about username vagrant has it's own convenient name (ubuntu/xenial64) ubuntu is your username and is your machine name 
### run porvision 
        to execute your inline shell script you can use the following  command 
        ```
            vagrant provision
        ```
        and should waiting until finish 
### run jenkins file
     now you can access jenkins from you guest machine open your browser and go to the following link 
        ```
            http://localhost:8080
        ```
    and continue to configure you jenkins and download your plugin
    